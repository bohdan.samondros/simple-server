package org.example;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import java.io.*;
import java.net.InetSocketAddress;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

public class Main {
    public static void main(String[] args) throws IOException {
        HttpServer server = HttpServer.create(new InetSocketAddress("localhost", 8001), 0);
        ThreadPoolExecutor threadPoolExecutor = (ThreadPoolExecutor) Executors.newFixedThreadPool(10);

        server.createContext("/test", new MyHttpHandler());
        server.setExecutor(threadPoolExecutor);
        server.start();
        System.out.println(" Server started on port 8001");
    }

    public static class MyHttpHandler implements HttpHandler {

        public void handle(HttpExchange exchange) throws IOException {
            switch (exchange.getRequestMethod()) {
                case "GET":
                    handleGet(exchange);
                    break;
                case "POST":
                    handlePost(exchange);
                    break;
                default:
                    exchange.sendResponseHeaders(405, 0);
                    exchange.getResponseBody().close();
            }
        }

        private void handleGet(HttpExchange exchange) throws IOException {
            System.out.println("URL: " + exchange.getRequestMethod() + " " + exchange.getRequestURI());
            System.out.println("Headers:");
            exchange.getRequestHeaders().forEach((k, v) -> System.out.println(k + "=" + v));

            OutputStream responseStream = exchange.getResponseBody();

            String response = "Hello World!";

            exchange.sendResponseHeaders(200, response.length());

            responseStream.write(response.getBytes());
            responseStream.flush();
            responseStream.close();
        }

        private void handlePost(HttpExchange exchange) throws IOException {
            System.out.println("URL: " + exchange.getRequestMethod() + " " + exchange.getRequestURI());
            System.out.println("Headers:");
            exchange.getRequestHeaders().forEach((k, v) -> System.out.println(k + "=" + v));

            InputStream requestStream = exchange.getRequestBody();

            BufferedReader reader = new BufferedReader(new InputStreamReader(requestStream));

            String line;
            while ((line = reader.readLine()) != null) {
                System.out.println(line);
            }

            exchange.sendResponseHeaders(200, 0);
            exchange.getResponseBody().close();
        }
    }
}
